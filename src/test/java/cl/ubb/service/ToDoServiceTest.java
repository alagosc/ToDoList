package cl.ubb.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyLong;



import cl.ubb.dao.ToDoDao;
import cl.ubb.model.ToDo;

import org.junit.Assert;
import org.junit.Before;


@RunWith(MockitoJUnitRunner.class)
public class ToDoServiceTest {
		
	@Mock
	private ToDoDao toDoDao;
		
	@InjectMocks
	private ToDoService toDoService;

	/*refactorización*/
	private ToDo miToDo;
	private ToDo resultado;
	
	@Before
	public void SetUp(){
		miToDo = new ToDo();
		miToDo.setId(1L);
	}
	
	@Test
	public void ProbarInstacia() {
		//arrange
		ToDoService toDoService = new ToDoService();
		
		//assert
		Assert.assertNotNull(toDoService);
		
	}

	@Test
	public void cuandoGuardoUnToDoDeberiaRetornarElMismoTodo(){
		//arrange
	/*	ToDo miToDo = new ToDo();
		miToDo.setId(1L);
		ToDo resultado = new ToDo();
		*/
		
		//act
		when(toDoDao.save(miToDo)).thenReturn(miToDo);
		resultado = toDoService.guardarElemento(miToDo);
		
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(resultado.getId(), 1L);
	}
	
	@Test
	public void cuandoEliminoToDoRetornarElToDoEliminado(){
		//arrange
		/*ToDo miToDo = new ToDo();
		miToDo.setId(2L);
		ToDo resultado = new ToDo();
		*/
		//act
		doNothing().when(toDoDao).delete(1L);
		resultado = toDoService.borrarElemento(miToDo);
		
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(resultado.getId(), 1L);
		verify(toDoDao).delete(1L);
	}
	
	@Test (expected = IllegalArgumentException.class )
	public void cuandoEliminoUnToDoNullLanzaExcepcion() throws Exception{
		//arrange
		ToDo toDoEliminar = null;
		
		//act
		Mockito.doThrow(new IllegalArgumentException()).when(toDoDao).delete(toDoEliminar);
		toDoService.borrarElemento(toDoEliminar);	
	}
	
	@Test
	public void obtenterTodosLosToDo() throws Exception{
		//arrange
		List<ToDo> misToDos = new ArrayList<ToDo>();
		List<ToDo> resultado = new ArrayList<ToDo>();
		
		//act
		when(toDoDao.findAll()).thenReturn(misToDos);
		resultado = toDoService.obtenerTodos();
		
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(resultado, misToDos);
	}
	
	@Test
	public void obtenerToDoPorCategoria(){
		//arrange
		List<ToDo> misToDos = new ArrayList<ToDo>();
		List<ToDo> resultado = new ArrayList<ToDo>();
		
		//act
		when(toDoDao.findByCategoria(anyString())).thenReturn(misToDos);
		resultado = toDoService.obtenerPorCategoria("trabajo");
		
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(resultado, misToDos);
		
	}

	@Test
	public void obtenerToDoPorId(){
		//arrange
		/*ToDo miToDo = new ToDo();
		miToDo.setId(1L);
		ToDo resultado = new ToDo();
		*/
		//act
		when(toDoDao.findToDoById(anyLong())).thenReturn(miToDo);
		resultado = toDoService.obtenerToDoPorId(miToDo.getId());
		
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(resultado.getId(), miToDo.getId());

	}
	
	@Test (expected = ToDoService.noExisteToDoExcepcion.class) //assert
	public void lanzaExcepcionCuandoNoHayToDoAObtenerPorId() throws Exception{
		
		//act
		when(toDoDao.findToDoById(0L)).thenReturn(null);
		toDoService.obtenerToDoPorId(0);
		
	}
	
	@Test
	public void cuandoActualizaUnToDoRetornaUnToDoActualizado(){
		//arrange
		ToDo miToDo = new ToDo();
		ToDo resultado = new ToDo();
		
		miToDo.setCategoria("Trabajo");
		miToDo.setDescripcion("revisar test");
		miToDo.setEstado(true);
		
		ToDo toDoActualizado = miToDo;
		toDoActualizado.setEstado(false);
		
		when(toDoDao.save(miToDo)).thenReturn(toDoActualizado);
		resultado = toDoService.actualizarToDo(miToDo);
		
		Assert.assertNotNull(resultado);
		Assert.assertEquals(resultado.isEstado(), false);
	}
}
