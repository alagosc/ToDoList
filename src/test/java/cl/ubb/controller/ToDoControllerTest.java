package cl.ubb.controller;

import static org.junit.Assert.*;

import static org.mockito.Matchers.anyLong;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.model.ToDo;
import cl.ubb.service.ToDoService;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;


@RunWith(MockitoJUnitRunner.class)
public class ToDoControllerTest {

	@Mock
	private ToDoService toDoService;
	
	@InjectMocks
	private ToDoController toDoController;
	
	@Test
	public void crearToDo() {
		RestAssuredMockMvc.standaloneSetup(toDoController);
		ToDo miToDo = new ToDo();
		miToDo.setId(1L);
		miToDo.setTitulo("trabajo");
		miToDo.setEstado(false);
		
		Mockito.when(toDoService.guardarElemento(Matchers.any(ToDo.class))).thenReturn(miToDo);
		
		given().
			contentType("application/json").
			body(miToDo).
		when().
			post("/toDo/crearToDo").
		then().
			body("titulo", equalTo(miToDo.getTitulo())).
			statusCode(201);
	}
	
	@Test
	public void eliminarToDo(){

		RestAssuredMockMvc.standaloneSetup(toDoController);
		ToDo miToDo = new ToDo();
		miToDo.setTitulo("trabajo");
		
		Mockito.when(toDoService.obtenerToDoPorId(anyLong())).thenReturn(miToDo);
		Mockito.when(toDoService.borrarElemento(Matchers.any(ToDo.class))).thenReturn(miToDo);
		
		given().
			contentType("application/json").
		when().
			delete("/toDo/eliminarToDo/{idToDo}",1).	
		then().
			body("titulo", equalTo("trabajo")).
			statusCode(200);
	}
	
	@Test
	public void obtenerUnToDo(){
		RestAssuredMockMvc.standaloneSetup(toDoController);
		ToDo miToDo = new ToDo();
		miToDo.setId(1L);
		
		Mockito.when(toDoService.obtenerToDoPorId(anyLong())).thenReturn(miToDo);
		
		given().
			contentType("application/json").
		when().
			get("/toDo/obtenerToDo/{idToDo}",1).
		then().
			body("id", is(1)).
			statusCode(200);
	}
}
