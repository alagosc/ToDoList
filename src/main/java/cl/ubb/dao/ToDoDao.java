package cl.ubb.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.ToDo;

//
public interface ToDoDao extends CrudRepository<ToDo, Long> {

	@Query("select t from ToDo t where categoria = ?1")
	List<ToDo> findByCategoria(String categoria);

	@Query("select t from ToDo t where id = ?1")
	ToDo findToDoById(long anyLong);
}
