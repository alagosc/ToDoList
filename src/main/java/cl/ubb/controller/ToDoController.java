package cl.ubb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;

import cl.ubb.model.ToDo;
import cl.ubb.service.ToDoService;

@RequestMapping("/toDo")
@RestController
public class ToDoController {
	
	@Autowired
	private ToDoService toDoService;
	
	
	@RequestMapping (value = "/crearToDo", method = POST)
	@ResponseBody
	public ResponseEntity<ToDo>CrearToDo(@RequestBody ToDo toDo){
		toDoService.guardarElemento(toDo);
		return new ResponseEntity<ToDo>(toDo, HttpStatus.CREATED);
		
	}
	
	@RequestMapping (value = "/eliminarToDo/{idToDo}", method = DELETE)
	@ResponseBody
	public ResponseEntity<ToDo>eliminarToDo(@PathVariable("idToDo") long idToDo){
		ToDo miToDo = new ToDo();
		
		miToDo = toDoService.obtenerToDoPorId(idToDo);
		
		toDoService.borrarElemento(miToDo);
		return new ResponseEntity<ToDo>(miToDo, HttpStatus.OK);
	}
	
	@RequestMapping (value = "/obtenerToDo/{idToDo}", method = GET)
	public ResponseEntity<ToDo>obtenerToDo(@PathVariable("idToDo") long idToDo){
		
		return new ResponseEntity<ToDo>(toDoService.obtenerToDoPorId(idToDo), HttpStatus.OK);
		
	}
}
