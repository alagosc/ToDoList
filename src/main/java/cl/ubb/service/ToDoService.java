package cl.ubb.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.ubb.dao.ToDoDao;
import cl.ubb.model.ToDo;

@Service
public class ToDoService {

	
	@Autowired
	private ToDoDao toDoDao;
	
	public ToDo guardarElemento(ToDo myToDo){
		
		return toDoDao.save(myToDo);
	}

	public ToDo borrarElemento(ToDo myToDo) {
		// TODO Auto-generated method stub
		if(myToDo == null)
			throw new IllegalArgumentException();
		
		toDoDao.delete(myToDo.getId());
		return myToDo;
		
	}

	public ArrayList<ToDo> obtenerTodos() {
		// TODO Auto-generated method stub
		ArrayList<ToDo> misToDos = new ArrayList<ToDo>();
		misToDos = (ArrayList<ToDo>) toDoDao.findAll();
		return misToDos ;
	}

	public List<ToDo> obtenerPorCategoria(String categoria) {
		// TODO Auto-generated method stub
		List<ToDo> misToDos = new ArrayList<ToDo>();
		misToDos = (List<ToDo>) toDoDao.findByCategoria(categoria); 
		return misToDos;
	}

	public ToDo obtenerToDoPorId(long id) {
		// TODO Auto-generated method stub
		 ToDo miToDo = new ToDo();
		
		 miToDo = toDoDao.findToDoById(id);
		
		 if (miToDo == null){
			 throw new noExisteToDoExcepcion();
		 }else{
			 return miToDo; 
		 }
		 
	
	}
	
	public class noExisteToDoExcepcion extends RuntimeException{}

	public ToDo actualizarToDo(ToDo miToDo) {
		// TODO Auto-generated method stub
		return toDoDao.save(miToDo);
	}

	
}
